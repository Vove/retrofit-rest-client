// This is a generated file. Not intended for manual editing.
package cn.vove7.plugin.rest.psi;

import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.PsiElement;

public class RestVisitor extends PsiElementVisitor {

  public void visitComments(@NotNull RestComments o) {
    visitPsiElement(o);
  }

  public void visitEBad(@NotNull RestEBad o) {
    visitPsiElement(o);
  }

  public void visitEHeader(@NotNull RestEHeader o) {
    visitPsiElement(o);
  }

  public void visitEMethod(@NotNull RestEMethod o) {
    visitPsiElement(o);
  }

  public void visitEParam(@NotNull RestEParam o) {
    visitPsiElement(o);
  }

  public void visitESeparator(@NotNull RestESeparator o) {
    visitPsiElement(o);
  }

  public void visitEUrl(@NotNull RestEUrl o) {
    visitPsiElement(o);
  }

  public void visitHeaders(@NotNull RestHeaders o) {
    visitPsiElement(o);
  }

  public void visitOptions(@NotNull RestOptions o) {
    visitPsiElement(o);
  }

  public void visitParams(@NotNull RestParams o) {
    visitPsiElement(o);
  }

  public void visitRequest(@NotNull RestRequest o) {
    visitPsiElement(o);
  }

  public void visitRequestBody(@NotNull RestRequestBody o) {
    visitPsiElement(o);
  }

  public void visitResponse(@NotNull RestResponse o) {
    visitPsiElement(o);
  }

  public void visitResponseBody(@NotNull RestResponseBody o) {
    visitPsiElement(o);
  }

  public void visitWs(@NotNull RestWs o) {
    visitPsiElement(o);
  }

  public void visitPsiElement(@NotNull PsiElement o) {
    visitElement(o);
  }

}
