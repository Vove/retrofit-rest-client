// This is a generated file. Not intended for manual editing.
package cn.vove7.plugin.rest.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface RestParams extends PsiElement {

  @NotNull
  List<RestEParam> getEParamList();

  @NotNull
  List<RestWs> getWsList();

}
